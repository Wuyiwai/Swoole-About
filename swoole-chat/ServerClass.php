<?php 
	class Server
	{
		private $serv;
		public function __construct()
		{
			$this->serv = new swoole_websocket_server("127.0.0.1", 9501);
			$this->serv->on('open',    array($this, 'onOpen'));
			$this->serv->on('message', array($this, 'onMessage'));
			$this->serv->on('Close',   array($this, 'onClose'));
			$this->serv->on('request', array($this, 'onRequest'));
			$this->serv->start();
		}

		//当WebSocket客户端与服务器建立连接并完成握手后会回调此函数
		public function onOpen($serv, $request)
		{
			echo "服务端：当前已与   socket:".$request->fd."   的客户端握手成功\n";
		}

		//当服务器收到来自客户端的数据帧时会回调此函数。
		//$frame->fd:客户端的socket id
		public function onMessage($serv, $frame)	
		{
			echo "服务端：socket为  ".$frame->fd."   的客户端发来的数据为   ".$frame->data."\n";
			$serv->push($frame->fd, "服务器已经收到信息");
		}

		public function onRequest($request, $response)
		{
			foreach ($this->serv->connections as $fd) 
			{
				$this->serv->push($fd,$request->get['message']);
			}
		}

		public function onClose($serv, $fd)
		{
			echo "服务端：socket为   {$fd}   的客户端关闭了连接\n\n";
		}

	}

	$server = new Server();
?>