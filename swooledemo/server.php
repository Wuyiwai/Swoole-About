<?php 
class Server
{
	private $serv;
	public function __construct()
	{
		$this->serv = new swoole_server("0.0.0.0", 9501);
		$this->serv->set(
			array(
				'worker_num'    => 8,
				'daemonize'     => false,
				'max_request'   =>10000,
				'dispatch_mode' => 2,
				'debug_mode'    => 1
			)
		);

		$this->serv->on('Start',   array($this, 'onStart'));
		$this->serv->on('Connect', array($this, 'onConnect'));
		$this->serv->on('Receive', array($this, 'onReceive'));
		$this->serv->on('Close',   array($this, 'onClose'));
		$this->serv->start();
	}

	public function onStart($serv)
	{
		echo "服务端开始运行\n";
	}

	public function onConnect($serv, $fd, $from_id)
	{
		$serv->send($fd,"你好，{$fd}!");
	}

	public function onReceive(swoole_server $serv, $fd, $from_id, $data)
	{
		echo "收到来自客户端  {$fd}  的消息：   {$data}\n";
	}

	public function onClose($serv, $fd, $from_id)
	{
		echo "客户端   {$fd}   关闭了链接\n\n\n";
	}
}

$server = new Server();
?>